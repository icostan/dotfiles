#!/usr/bin/env sh

set -e

SSH_KEY=id_rsa
SSH_ADD=/usr/bin/ssh-add
USB_LABEL=keys
USB_MOUNT=/run/media/$USER/$USB_LABEL
UMOUNT_BIN=/usr/bin/udiskie-umount

$SSH_ADD $USB_MOUNT/$SSH_KEY
notify-send "Added $SSH_KEY to ssh-agent!"

$UMOUNT_BIN $USB_MOUNT
