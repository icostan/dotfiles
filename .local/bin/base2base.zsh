#!/usr/bin/env zsh

if [ $# != 2 ]
then
    echo
    echo Usage: base2base.zsh NUMBER BASE
    echo
    exit 0
fi

echo "Number $1 to base $2..."

echo $(([#$2] $1))
