# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt beep nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/icostan/.zshrc'

autoload -Uz compinit
compinit

zstyle ':completion:*' menu select
#zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
# End of lines added by compinstall

# bindkeys
bindkey -e
bindkey '^w' forward-word
bindkey '^b' backward-word
bindkey '^d' backward-kill-word
bindkey '^r' history-incremental-search-backward
bindkey '^p' up-history
bindkey '^n' down-history

# history navigation
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# prompt
autoload -Uz promptinit
promptinit
PROMPT='%B%F{magenta}%1~%f%b %% '
RPROMPT='[%F{yellow}%?%f]'

# syntax, aliases and auto-suggestions
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/alias-tips/alias-tips.plugin.zsh

# ZLE
source ~/.zsh/zle.zsh

# antibody
# run 'antibody bundle < ~/.zsh_plugins.txt > ~/.zsh_plugins.sh'
source ~/.zsh_plugins.sh
