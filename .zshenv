# exports
source $HOME/.exports

# aliases
source $HOME/.aliases

# secrets
source $HOME/.secrets

# rbenv
eval "$(rbenv init -)"

# nvm
source /usr/share/nvm/init-nvm.sh
